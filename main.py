import serial
import time
import requests
import locale

locale.setlocale(locale.LC_ALL, "ru")

# Параметры COM - подключения
com_port = 'COM4'
baud_rate = 9600
byte_size = 8
pari_ty = 'N'
stop_bits = 1

# API сайта с прогнозом погоды
url = 'http://api.openweathermap.org/data/2.5/weather'
params = {
    'id': 524699,
    'appid': '7977c58f8e55136860befb9c1354ddcc',
    'mode': 'json',
    'lang': 'ru',
    'units': 'metric'}

def wind_d(wind_deg):
    if wind_deg == 0:
        return 'Сев'
    elif 0 < wind_deg < 90:
        return 'С-З'
    elif wind_deg == 90:
        return 'Зап'
    elif 90 < wind_deg < 180:
        return 'Ю-З'
    elif wind_deg == 180:
        return 'Южн'
    elif 180 < wind_deg < 270:
        return 'Ю-В'
    elif wind_deg == 270:
        return 'Вост'
    else:
        return 'С-В'

def weather_condition(id_description):
    if 200 <= id_description < 300: # Гроза
        thunderstorm()
    elif 300 <= id_description < 600: # Дождь или Моросит
        rain()
    elif 600 <= id_description < 700: # Снег
        snow(5)
    elif 700 <= id_description < 800: # Туман
        mist(5)
    elif id_description == 800: # Ясно
        clear_clouds()
    else: # Облачно
        clouds(5)

def snow(t):
    for i in range(t):
        ser.write(b'\x0c')
        ser.write(b'\x1B\x5B\x48')
        ser.write(b'\x8f\x20\x20\x9f\x20\x20\x8f\x20\x20\x9f\x20\x20\x8f\x20\x20\x9f\x20\x20\x8f')
        ser.write(b'\x1B\x6C\x02\x02')
        ser.write(b'\x9f\x20\x20\x20\x20\x20\x9f\x20\x20\x20\x20\x20\x9f\x20\x20\x20\x20\x20\x9f')
        time.sleep(1)
        ser.write(b'\x0c')
        ser.write(b'\x1B\x5B\x48')
        ser.write(b'\x9f\x20\x20\x20\x20\x20\x9f\x20\x20\x20\x20\x20\x9f\x20\x20\x20\x20\x20\x9f')
        ser.write(b'\x1B\x6C\x02\x02')
        ser.write(b'\x20\x20\x20\x8f\x20\x20\x20\x20\x20\x8f\x20\x20\x20\x20\x20\x8f')
        time.sleep(1)
        ser.write(b'\x0c')
        ser.write(b'\x1B\x6C\x02\x02')
        ser.write(b'\x8f\x20\x20\x9f\x20\x20\x8f\x20\x20\x9f\x20\x20\x8f\x20\x20\x9f\x20\x20\x8f')
        ser.write(b'\x1B\x5B\x48')
        ser.write(b'\x20\x20\x20\x8f\x20\x20\x20\x20\x20\x8f\x20\x20\x20\x20\x20\x8f')
        time.sleep(1)

def thunderstorm():
    pass

def rain():
    pass

def mist(t):
    ser.write(b'\x1B\x5B\x48')
    ser.write(b'\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d')
    ser.write(b'\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d')
    ser.write(b'\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d')
    ser.write(b'\x8d\x9d\x8d\x9d\x8d\x9d\x8d\x9d')
    time.sleep(t)

def clear_clouds():
    pass

def clouds(t):
    ser.write(b'\x0c')
    ser.write(b'\x1B\x5B\x48')
    for i in range(t):
        ser.write(b'\x1B\x5B\x48')
        for s in range(i):
            ser.write(b'\x1B\x5B\x43')
        ser.write(b'\x8d\x9d\x20\x20\x20\x8d\x9d\x20\x20\x20\x20\x20\x20\x8d\x9d')
        ser.write(b'\x1B\x5B\x42')
        ser.write(b'\x1B\x5B\x4C')
        for s in range(i):
            ser.write(b'\x1B\x5B\x43')
        ser.write(b'\x20\x8d\x9d\x20\x20\x20\x20\x20\x8d\x9d\x20\x20\x20\x20\x20\x8d\x9d')
        time.sleep(2)
        ser.write(b'\x0c')

def weath(url, params):
    r = requests.get(url, params=params) # Вытаскиваем данные из сайта
    if r.status_code == 200:
        weather = r.json() # Данные в формате json
        print(r.text)
        temp = weather['main']['temp'] # Температура воздуха
        feels_like = weather['main']['feels_like'] # Ощущение температуры воздуха
        pressure = int(weather['main']['pressure'] * 0.75) # Давление
        humidity = weather['main']['humidity'] # Влажность
        wind = weather['wind']['speed'] # Скорость ветра
        wind_deg = wind_d(weather['wind']['deg']) # Направление ветра
        description = weather['weather'][0]['description'] # Погодные условия (снег, дождь, туман и т.д.)
        id_description = weather['weather'][0]['id']

def time_vision():
    ser.write(b'\x0c')
    ser.write(f'\x1B\x51\x42{time.strftime("%A %d %B")}\x0D'.encode('cp1251'))
    for i in range(10):
        ser.write(f'\x1B\x51\x41{time.strftime("%X")}\x0D'.encode('cp1251'))
        time.sleep(1)



ser = serial.Serial(port=com_port, baudrate=baud_rate, bytesize=byte_size,
                    parity=pari_ty, stopbits=stop_bits, timeout=None)


ser.write(b'\x1B\x40') # Инициализация дисплея
ser.write(b'\x0c') # Очистка дисплея
ser.write(b'\x1B\xF3\x01') # Ширина экрана 0 - 20х2, 1 - 22х2
ser.write(b'\x1B\xF5\x01') # Кодировка х00 - ср866, х01 - win1251
ser.write(b'\x1B\x25\x01') # Разрешаем использование пользовательских символов

ser.write(b'\x1B\x26\x01\x8f\x8f\x50\x20\x70\x20\x00\x00') # Снежинка в верхнем регистре
ser.write(b'\x1B\x26\x01\x9f\x9f\x50\x00\x00\x02\x07\x02') # Снежинка в нижнем регистре
# ser.write(b'\x1B\x26\x01\x8e\x8e\x50\x55\xAA\x55\xAA\x55') # Решетка
# ser.write(b'\x1B\x26\x01\x9e\x9e\x50\xFF\xFF\xFF\xFF\xFF') # Темное облако
ser.write(b'\x1B\x26\x01\x8d\x8d\x50\x37\x4D\x61\x2A\x5A') # Левое облако
ser.write(b'\x1B\x26\x01\x9d\x9d\x50\x21\x4A\x51\x2C\x3B') # Правое облако

while True:
    r = requests.get(url, params=params) # Вытаскиваем данные из сайта
    if r.status_code == 200:
        weather = r.json() # Данные в формате json
        print(r.text)
        temp = weather['main']['temp'] # Температура воздуха
        feels_like = weather['main']['feels_like'] # Ощущение температуры воздуха
        pressure = int(weather['main']['pressure'] * 0.75) # Давление
        humidity = weather['main']['humidity'] # Влажность
        wind = weather['wind']['speed'] # Скорость ветра
        wind_deg = wind_d(weather['wind']['deg']) # Направление ветра
        description = weather['weather'][0]['description'] # Погодные условия (снег, дождь, туман и т.д.)
        id_description = weather['weather'][0]['id']
        print(id_description)
        print(type(id_description))
        for i in range(60):
            ser.write(f'\x1B\x51\x41Температура {temp}C\x0D'.encode('cp1251'))
            ser.write(f'\x1B\x51\x42Ощущается как {feels_like}\x0D'.encode('cp1251'))
            time.sleep(3)
            ser.write(f'\x1B\x51\x41Давление {pressure} мм.рт.ст\x0D'.encode('cp1251'))
            ser.write(f'\x1B\x51\x42Влажность {humidity}%\x0D'.encode('cp1251'))
            time.sleep(3)
            ser.write(f'\x1B\x51\x41Ветер {wind_deg} {wind}м/с\x0D'.encode('cp1251'))
            ser.write(f'\x1B\x51\x42{description}\x0D'.encode('cp1251'))
            time.sleep(3)
            weather_condition(id_description)
            time_vision()
    else:
        for i in range(50):
            time_vision()
            ser.write(f'\x1B\x51\x41Ошибка загрузки\x0D'.encode('cp1251'))
            ser.write(f'\x1B\x51\x42данных о погоде\x0D'.encode('cp1251'))
            time.sleep(2)



# ser.close()
